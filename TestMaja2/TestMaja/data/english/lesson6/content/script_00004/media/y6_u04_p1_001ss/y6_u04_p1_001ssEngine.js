//namespace
window.y6u04p1001ss = window.y6u04p1001ss||{};

(function(wnd){
	
	//constructor
	var Engine = function(resource, stage){
		if(ydpjs == null && wnd.y6u04p1001ss.ydpjs != undefined){
			ydpjs = wnd.y6u04p1001ss.ydpjs;
		}
		
		this.lib = wnd.y6u04p1001ss;
		this.initialize(resource, stage);
	}
	
	var p = Engine.prototype;
	
	//ydpjs lib namespace
	var ydpjs = null;
	
	p.stage = null;
	
	//Representative of fla file
	p.resource = null;
	
	//fla graphic library object
	p.lib = null;
	
	p.initialize = function(resource, stage){
		this.resource = resource;
		this.stage = stage;
		this.stage.enableMouseOver();
		Touch.enable(this.stage)
		
		
		
		// --- SETUP ---
		
		this.photo_number = 3;
		
		this.photo = ["image01", "image02", "image03"];
		
		this.audio = [ ["sound01a", "sound01b", "sound01c", "sound01d", "sound01e"], ["sound02a", "sound02b", "sound02c", "sound02d", "sound02e"], ["sound03a", "sound03b", "sound03c", "sound03d", "sound03e"] ];
		
		this.photo_size = [640, 360];
		
		this.audio_interval = [[500], [500], [500], [500], [500], [500], [500], [500], [500], [500]]; // miliseconds
	
		// --- SETUP END ---	
		
		
		
		this.currentSlide = 1;
		this.isPlaying = false;
		this.dotsArr = [];
		this.allSounds = [];
		this.allAudioCounter;
		this.allSoundsInstance;
		this.soundPos = 0;
		this.currentSlideAudio = 0;
		this.abc = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"];
		this.gameState = "STOP";
		this.soundsArr = [];
		this.delayID;
		
		this.photoFrame = new Graphics();
		this.photoFrame.setStrokeStyle(2);
		this.photoFrame.beginStroke("#2E677B");
		this.photoFrame.drawRect(1,1,this.photo_size[0] - 2, this.photo_size[1] - 2);
		this.photoFrame.endStroke();
		var s = new Shape(this.photoFrame);
		this.resource.addChild(s);
		
		this.shiftX = Math.round((930 - this.photo_size[0]) / 2);
		this.resource.imgHolder = new MovieClip();
		this.resource.imgHolder.x = this.shiftX;
		this.resource.addChildAt(this.resource.imgHolder, 0);
		
		s.x = this.shiftX;
		
		for(var i=0;i<this.photo_number;i++) {
			var dBtn = new y6u04p1001ss.dot_button;
			dBtn.stop();
			dBtn.x = this.resource.imgHolder.x + this.photo_size[0] - 39 - (i * 48);
			dBtn.y = this.photo_size[1] + 11;
			dBtn.slideID = this.photo_number - i;
			dBtn.onPress = ydpjs.Delegate.create(this, this.dotPress);
			dBtn.onMouseOver = ydpjs.Delegate.create(this, this.onBtnOver2);
			dBtn.onMouseOut = ydpjs.Delegate.create(this, this.onBtnOut2);
			this.resource.addChild(dBtn);
			this.dotsArr.push(dBtn);
			
			var tArr = this.audio[i].slice();
			this.allSounds.concat(tArr);
		}
		
		this.dotsArr.reverse();
		
		this.resource.btnPlay.onPress = ydpjs.Delegate.create(this, this.playPauseSlide);
		this.resource.btnPlay.onMouseOver = ydpjs.Delegate.create(this, this.onBtnOver);
		this.resource.btnPlay.onMouseOut = ydpjs.Delegate.create(this, this.onBtnOut);
		this.resource.btnPlay.stop();
		this.resource.btnPlay.bg = this.resource.btnPlay.bg0;
		this.resource.btnPlay.bg.stop();
		this.resource.btnPlay.x = this.shiftX;
		
		this.resource.btnStop.onPress = ydpjs.Delegate.create(this, this.stopSlide);
		this.resource.btnStop.onMouseOver = ydpjs.Delegate.create(this, this.onBtnOver);
		this.resource.btnStop.onMouseOut = ydpjs.Delegate.create(this, this.onBtnOut);
		this.resource.btnStop.bg = this.resource.btnStop.bg0;
		this.resource.btnStop.bg.stop();
		this.resource.btnStop.x = this.shiftX + 75;
		
		this.addPageChangeDetector();
		this.showSlide();
	}
	
	
	
	
	p.addPageChangeDetector = function() {
		var pageButtonOnClick = this.onPageChange;
		var th = this;
		
		this.pBtnClick = function(evt){ pageButtonOnClick(evt, th) };
		
		this.btnHome = document.querySelectorAll(".qp-shape.emp_home_container.qp-shape-up")[0];
		this.btnPrev = document.querySelectorAll(".qp-prev-button.emp_button.qp-prev-button-up")[0];
		this.btnNext = document.querySelectorAll(".qp-next-button.emp_button.qp-next-button-up")[0];
		
		this.pageBtns = [this.btnHome, this.btnPrev, this.btnNext];
		
		for(var i=0;i<this.pageBtns.length;i++){
			var bt = this.pageBtns[i];
			if (bt) {
				bt.addEventListener("click", this.pBtnClick, false);
			}
		}
	}
	
	p.removePageChangeDetector = function(th) {
		for(var i=0;i<th.pageBtns.length;i++){
			var bt = th.pageBtns[i];
			if (bt) {
				bt.removeEventListener("click", th.pBtnClick, false);
			}
		}
	}
	
	p.onPageChange = function(evt, th) {
		th.stopAudio();
		th.removePageChangeDetector(th);
	}
	
	
	
	
	
	p.showSlide = function() {
		// clear all
		while(this.resource.imgHolder.getNumChildren()) {
			this.resource.imgHolder.removeChildAt(0);
		}
		// new image
		var img = new Bitmap( window.images.y6u04p1001ss[this.photo[this.currentSlide - 1]].src );
		this.resource.imgHolder.addChild(img);
	}
	
	p.playPauseSlide = function(e) {
		this.playAllAudio();
		this.selectDot();
	}
	
	p.stopSlide = function(e) {
		this.gameState = "STOP";
		this.stopAudio();
		this.currentSlide = 1;
		this.showSlide();
		this.selectedDot.gotoAndStop(0);
		this.selectedDot = null;
	}
	
	p.playAllAudio = function() {
		if (!this.isPlaying) {
			this.gameState = "PLAY";
			this.resource.btnPlay.gotoAndStop(1);
			this.resource.btnPlay.bg = this.resource.btnPlay.bg1;
			this.resource.btnPlay.bg.gotoAndStop(0);
			
			this.clearAllSounds();
						
			this.isPlaying = true;
			var soundID = "sound" + this.getSlideID() + this.abc[this.currentSlideAudio];
			
			//this.allSoundsInstance = SoundJS.play(soundID, SoundJS.INTERRUPT_EARLY, 0, this.soundPos, 0);
			this.allSoundsInstance = SoundJS.play(soundID, SoundJS.INTERRUPT_EARLY,0,0,0);
			this.allSoundsInstance.setPosition(this.soundPos);
			this.allSoundsInstance.onComplete = ydpjs.Delegate.create(this, this.onAllAudioEnd);			
			
		} else {
			this.gameState = "PAUSE";
			this.resource.btnPlay.gotoAndStop(0);
			this.resource.btnPlay.bg = this.resource.btnPlay.bg0;
			this.resource.btnPlay.bg.gotoAndStop(0);
			
			if (this.allSoundsInstance) {
				this.soundPos = this.allSoundsInstance.getPosition();
			}
			SoundJS.stop();
			this.isPlaying = false;
		}
	}
	
	p.onAllAudioEnd = function() {
		
		if (this.gameState != "PLAY")
			return;
		
		this.currentSlideAudio++;
		var soundID = "sound" + this.getSlideID() + this.abc[this.currentSlideAudio];

		if (this.audio[this.currentSlide - 1].indexOf(soundID) == -1) {
			this.currentSlideAudio=0;
			this.currentSlide++;
			var soundID = "sound" + this.getSlideID() + this.abc[this.currentSlideAudio];
		}

		if (this.currentSlide <= this.photo_number) {
			var audioDelay = this.audio_interval[this.currentSlide - 1];
			
			if (this.currentSlideAudio==0) {
				this.showSlide();
				this.selectDot();
			}
			
			this.delayID = setTimeout(this.playDelayedSound, audioDelay, this, soundID);
			
			//this.allSoundsInstance = SoundJS.play(soundID, SoundJS.INTERRUPT_ANY, audioDelay, 0, 0);
			//this.allSoundsInstance.onComplete = ydpjs.Delegate.create(this, this.onAllAudioEnd);
		} else {
			this.stopAudio();
			this.currentSlide = 1;
			this.showSlide();
			this.selectedDot.gotoAndStop(0);
			this.selectedDot = null;
		}
	}
	
	p.playDelayedSound = function(th, soundID) {
		th.allSoundsInstance = SoundJS.play(soundID, SoundJS.INTERRUPT_EARLY, 0, 0, 0);
		th.allSoundsInstance.onComplete = ydpjs.Delegate.create(th, th.onAllAudioEnd);
	}
	
	p.clearAllSounds = function() {
		SoundJS.stop();
		if (this.allSoundsInstance)
			delete this.allSoundsInstance.onComplete;
		clearTimeout(this.delayID);
	}

	p.stopAudio = function() {
		this.clearAllSounds();

		this.soundPos=0;
		this.resource.btnPlay.gotoAndStop(0);
		this.resource.btnPlay.bg = this.resource.btnPlay.bg0;
		this.resource.btnPlay.bg.gotoAndStop(0);
		this.isPlaying = false;
		this.currentSlideAudio = 0;
	}
	
	p.dotPress = function(e) {
		this.stopAudio();
		var dBtn = e.target;
		this.currentSlide = dBtn.slideID;
		this.selectDot();
		this.showSlide();
		this.playAllAudio();		
	}
	
	p.selectDot = function() {
		for(var i=0;i<this.dotsArr.length;i++){
			var dBtn = this.dotsArr[i];
			dBtn.gotoAndStop(0);
		}
		this.selectedDot = this.dotsArr[this.currentSlide - 1];
		this.selectedDot.gotoAndStop(2);
	}
	
	p.getSlideID = function() {
		var sldID = ((this.currentSlide < 10) ? ("0" + String(this.currentSlide)) : String(this.currentSlide));
		return sldID;
	}
	
	p.onBtnOver = function(e) {
		e.target.bg.gotoAndStop(1);
	}
	
	p.onBtnOut = function(e) {
		e.target.bg.gotoAndStop(0);
	}
	
	p.onBtnOver2 = function(e) {
		var dBtn = e.target;
		if(dBtn != this.selectedDot)
			dBtn.gotoAndStop(1);
	}
	
	p.onBtnOut2 = function(e) {
		var dBtn = e.target;
		if(dBtn != this.selectedDot)
			dBtn.gotoAndStop(0);
	}
	
	p.tick = function(){
		//Here place code which will execute in every frame		
		this.stage.update();
	}
	
	wnd.y6u04p1001ss.y6_u04_p1_001ssEngine = Engine;
	
}(window));