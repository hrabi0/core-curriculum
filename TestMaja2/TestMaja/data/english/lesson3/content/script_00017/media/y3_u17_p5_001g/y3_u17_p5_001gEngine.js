//namespace
window.y3u17p5001g = window.y3u17p5001g||{};

(function(wnd){
	
	//constructor
	var Engine = function(resource, stage){
		if(ydpjs == null && wnd.y3u17p5001g.ydpjs != undefined){
			ydpjs = wnd.y3u17p5001g.ydpjs;
		}
		
		this.lib = wnd.y3u17p5001g;
		this.initialize(resource, stage);
	}
	
	var p = Engine.prototype;
	
	//ydpjs lib namespace
	var ydpjs = null;
	
	p.stage = null;
	
	//Representative of fla file
	p.resource = null;
	
	//fla graphic library object
	p.lib = null;
	
	p.initialize = function(resource, stage){
		this.allOkText = "Well done!";
		this.allOKFeedMC = null;
		this.resource = resource;
		this.stage = stage;
		this.stage.enableMouseOver();
		Touch.enable(this.stage);
		
		
		this.floorSets = [[1,11], [2,12], [3,13], [4,14], [5,15], [6,16], [7,17], [8,18], [9,19], [10,20]];
		this.floors;
		this.currentFloor;
		this.currentLevel;
		this.currentRun;
		this.ticktack = 0;
		this.butsToTick = [];
		this.flick = true;
		this.delayed = true;
		this.intervalID;
		this.timeoutID;
		this.delayInitiated = false;
		this.soundOff = false;
		this.detectorAdded = false;
		
		for(var i=1;i<=10;i++){
			var bt = this.resource.btns["b" + i];
			var txt = (i < 6) ? String(i) : String(i + 5)
			var tf = new Text(txt, "bold 32px Trebuchet MS", "#000");
			tf.textAlign = "center";
			tf.x = bt.x - 44;
			tf.y = bt.y - 23;
			this.resource.btns.addChild(tf);
			
			bt.txt = tf;
			bt.onPress = ydpjs.Delegate.create(this, this.btPress);
			bt.onMouseOver = ydpjs.Delegate.create(this, this.btOver);
			bt.onMouseOut = ydpjs.Delegate.create(this, this.btOut);
			bt.floor = i;
			bt.clicked = false;
			bt.stop();
		}
		
		this.photoFrame = new Graphics();
		this.photoFrame.setStrokeStyle(2);
		this.photoFrame.beginStroke("#2E677B");
		this.photoFrame.drawRect(1,1,720 - 2, 430 - 2);
		this.photoFrame.endStroke();
		var s = new Shape(this.photoFrame);
		this.resource.addChild(s);
		
		
		this.resource.btns.startY = this.resource.btns.y;
		
		this.resource.startBtn.stop();
		this.resource.startBtn.onPress = ydpjs.Delegate.create(this, this.startPress);
		this.resource.startBtn.onMouseOver = ydpjs.Delegate.create(this, this.bOver);
		this.resource.startBtn.onMouseOut = ydpjs.Delegate.create(this, this.bOut);
		
		this.resource.reset.stop();
		this.resource.reset.onPress = ydpjs.Delegate.create(this, this.resetPress);
		this.resource.reset.onMouseOver = ydpjs.Delegate.create(this, this.bOver);
		this.resource.reset.onMouseOut = ydpjs.Delegate.create(this, this.bOut);
		
		var g = new Graphics();
		var s = new Shape(g);
		this.resource.progMC.addChildAt(s, 0);
		this.resource.progMC.g = g;
		this.resource.progMC.y -= 5;

		this.enableButtons(false);
		this.drawFloors();
		//this.initFloor();
		//this.addPageChangeDetector();
	}
	
	p.updateProgress = function() {
		var g = this.resource.progMC.g;
		var w = (this.currentLevel + 10 * this.currentRun) * (103 / (2 * this.floorSets.length));
		
		g.clear();
		g.beginFill(Graphics.getRGB(255,204,0));
		g.drawRoundRect(-1.5,-1.5,w,10,5);
		g.endFill();
		if (this.currentLevel == 0 && this.currentRun == 0) {
			this.resource.progMC.g.clear();
		}
	}

	p.addPageChangeDetector = function() {
		var pageButtonOnClick = this.onPageChange;
		var th = this;
		
		this.detectorAdded = true;
		this.pBtnClick = function(evt){ pageButtonOnClick(evt, th) };
		
		this.btnHome = document.querySelectorAll(".qp-shape.emp_home_container.qp-shape-up")[0];
		this.btnPrev = document.querySelectorAll(".qp-prev-button.emp_button.qp-prev-button-up")[0];
		this.btnNext = document.querySelectorAll(".qp-next-button.emp_button.qp-next-button-up")[0];
		
		this.pageBtns = [this.btnHome, this.btnPrev, this.btnNext];
		
		for(var i=0;i<this.pageBtns.length;i++){
			var bt = this.pageBtns[i];
			if (bt) {
				bt.addEventListener("click", this.pBtnClick, false);
			}
		}
	}
	
	p.removePageChangeDetector = function(th) {
		th.detectorAdded = false;
		for(var i=0;i<th.pageBtns.length;i++){
			var bt = th.pageBtns[i];
			if (bt) {
				bt.removeEventListener("click", th.pBtnClick, false);
			}
		}
	}
	
	p.onPageChange = function(evt, th) {
		th.stopGame(th);
	}
	
	p.stopGame = function(th) {
		/*
		th.removePageChangeDetector(th);
		th.soundOff = true;
		SoundJS.stop();
		clearTimeout(th.timeoutID);
		
		if (th.delayInitiated) {
			//console.log("here 0");
			th.initDelayedFloor(th);
		} else {
			//console.log("here 1");
			for(var i=1;i<=10;i++){
				var bt = th.resource.btns["b" + i];
				if (!bt.clicked) {
					bt.mouseEnabled = true;
				}
			}
		}
		*/
		
		th.resetPress(null);
	}
	
	p.drawFloors = function() {
		this.currentRun = 0;
		this.currentLevel = 0;
		this.floors = [[], []];
		for(var i=0;i<this.floorSets.length;i++){
			var id1 = Math.floor(Math.random() * 2);
			var id2 = (id1 == 1) ? 0 : 1;
			
			this.floors[0].push( this.floorSets[i][id1] );
			this.floors[1].push( this.floorSets[i][id2] );
		}		
	}
	
	p.initFloor = function() {
		this.updateProgress();
		if (this.currentLevel == 0 || this.currentLevel == 5) {
			
			if (!this.delayed) {
				this.delayed = true;
				this.delayInitiated = true;
				this.addDelay();
				return;
			}
			
			for(var i=1;i<=10;i++){
				var bt = this.resource.btns["b" + i];
				bt.txt.text = (i < 6) ? String(i + this.currentLevel) : String(i  + this.currentLevel + 5);
				bt.clicked = false;
				bt.txt.color = "#000000";
				bt.gotoAndStop(0);
				bt.txt.x = bt.x - 44;
			}

		}
		
		this.delayed = false;

		if (this.currentLevel == 10) {
			//this.unclickButtons();
			this.currentLevel = 0;
			this.currentRun++;
			
			if (this.currentRun == 2) {
				this.onAllOkEnd();
				//this.drawFloors();
				//this.initFloor();
			} else {
				this.initFloor();
			}
		} else {
			this.currentFloor = this.floors[this.currentRun][this.currentLevel];
			this.currentLevel++;
			var sndID = (this.currentFloor < 10) ? ("0" + String(this.currentFloor)) : String(this.currentFloor);
			var sndName = "y3_u17_p1_0" + sndID + "au";
			if (!this.soundOff) {
				this.timeoutID = setTimeout(this.playCurrentSound, 50, this, sndName);
				//var instance = SoundJS.play(sndName, SoundJS.INTERRUPT_EARLY, 750, 0, 0);
				//instance.onComplete = ydpjs.Delegate.create(this, this.enableButtons, true);
			} else {
				this.enableButtons(true);
			}
			var lev = (this.currentLevel < 6) ? this.currentLevel : this.currentLevel - 5;
			
			this.butsToTick[0] = this.resource.btns["b" + lev];
			this.butsToTick[1] = this.resource.btns["b" + (lev + 5)];
		}
	}
	
	p.onAllOkEnd = function() {
		this.allOKFeedMC = new y3u17p5001g.allOkFeedback();
		this.resource.addChild(this.allOKFeedMC);
		this.allOKFeedMC.x = 720/2;
		this.allOKFeedMC.y = 430/2;
		
		var tf = new Text(this.allOkText, "28px Arial", "#000000");
		tf.textAlign = "center";
		tf.y = 8;
		this.allOKFeedMC.addChild(tf);
		SoundJS.stop();
		playSound("allOk", 0);
	}

	p.playCurrentSound = function(th, sndName) {
		SoundJS.stop();
		var instance = SoundJS.play(sndName, SoundJS.INTERRUPT_EARLY, 0, 0, 0);
		instance.onComplete = ydpjs.Delegate.create(th, th.enableButtons, true);
	}
	
	p.addDelay = function(){
		this.intervalID = setInterval(this.initDelayedFloor, 2000, this);
	}
	
	p.initDelayedFloor = function(th){
		clearInterval(th.intervalID);
		th.delayInitiated = false;
		th.initFloor();
	}

	p.enableButtons = function(b){
		for(var i=1;i<=10;i++){
			var bt = this.resource.btns["b" + i];
			if (!bt.clicked) {
				bt.mouseEnabled = b;
			}
		}
	}
	
	p.unclickButtons = function(){
		for(var i=1;i<=10;i++){
			var bt = this.resource.btns["b" + i];
			bt.clicked = false;
			bt.txt.color = "#000000";
			bt.txt.x = bt.x - 44;
			bt.gotoAndStop(0);
		}
	}
	
	p.btPress = function(e){
		var bt = e.target;
		
		this.soundOff = false;
		if (!this.detectorAdded) {
			this.addPageChangeDetector();
		}
		
		if (Number(bt.txt.text) == this.currentFloor) {
			
			this.butsToTick[0].gotoAndStop(0);
			this.butsToTick[1].gotoAndStop(0);
			this.butsToTick = [];
			
			playSound("ok");
			
			bt.mouseEnabled = false;
			bt.clicked = true;
			bt.txt.color = "#FF0000";
			bt.txt.x = bt.x - 34
			bt.gotoAndStop(2);
			
			this.initFloor();

			this.enableButtons(false);
		} else {
			playSound("wrong");
		}
		
	}
	
	p.btOver = function(e){
		var bt = e.target;
		if (bt.mouseEnabled)
			bt.txt.color = "#FF0000";
	}
	p.btOut = function(e){
		var bt = e.target;
		if (bt.mouseEnabled)
			bt.txt.color = "#000000";
	}
	
	p.startPress = function(e) {
		this.resource.startBtn.mouseEnabled = false;
		this.resource.startBtn.alpha = 0.5;
		this.resource.startBtn.gotoAndStop(0);
		this.initFloor();
		
		if (!this.detectorAdded) {
			this.addPageChangeDetector();
		}
	}
	
	p.resetPress = function(e){
		
		this.resource.startBtn.mouseEnabled = true;
		this.resource.startBtn.alpha = 1;
		this.resource.startBtn.gotoAndStop(0);
		
		clearInterval(this.intervalID);
		clearTimeout(this.timeoutID);
		SoundJS.stop();

		this.resource.removeChild(this.allOKFeedMC);

		this.resource.progMC.g.clear();
		
		if (this.detectorAdded) {
			this.removePageChangeDetector(this);
		}
		
		this.soundOff = false;
		this.delayed = false;
		this.delayInitiated = false;
		this.unclickButtons();
		this.butsToTick = [];
		this.drawFloors();
		//this.initFloor();
		for(var i=1;i<=10;i++){
			var bt = this.resource.btns["b" + i];
			bt.txt.text = (i < 6) ? String(i + this.currentLevel) : String(i  + this.currentLevel + 5);
			bt.clicked = false;
			bt.txt.color = "#000000";
			bt.gotoAndStop(0);
			bt.txt.x = bt.x - 44;
		}

	}
	
	p.bOver = function(e) {
		var bt = e.target;
		if (bt.mouseEnabled)
			bt.gotoAndStop(1);
	}
	
	p.bOut = function(e) {
		var bt = e.target;
		if (bt.mouseEnabled)
			bt.gotoAndStop(0);
	}
	
	p.tick = function(){
		this.stage.update();
		
		this.ticktack++;
		
		if (this.ticktack == 5) {
			for (var i=0;i<this.butsToTick.length;i++){
				this.butsToTick[0].gotoAndStop(this.flick ? 0 : 1);
				this.butsToTick[1].gotoAndStop(this.flick ? 1 : 0);
			}
			this.flick = !this.flick;
			this.ticktack = 0;
		}
	}
	
	wnd.y3u17p5001g.y3_u17_p5_001gEngine = Engine;
	
}(window));