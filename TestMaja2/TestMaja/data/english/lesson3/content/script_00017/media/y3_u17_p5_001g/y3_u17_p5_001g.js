if (!window.y3u17p5001g) { window.y3u17p5001g = {}; }

var p; // shortcut to reference prototypes

// stage content:
(window.y3u17p5001g.y3_u17_p5_001g = function() {
	this.initialize();

	// Layer 2
	this.startBtn = new window.y3u17p5001g.start();
	this.startBtn.setTransform(630,30,1,1,0,0,0,35,25);

	this.reset = new window.y3u17p5001g.reset();
	this.reset.setTransform(690,30,1,1,0,0,0,25,25);

	this.progMC = new window.y3u17p5001g.progress_bar();
	this.progMC.setTransform(655,68,1,1,0,0,0,50,3);

	// Layer 4
	this.btns = new window.y3u17p5001g.btns();
	this.btns.setTransform(361,2.6,1,1,0,0,0,122,419.5);

	// Layer 5
	this.instance = new window.y3u17p5001g.bg();

	this.addChild(this.instance,this.btns,this.reset,this.startBtn,this.progMC);
}).prototype = p = new Container();
p.nominalBounds = new Rectangle(0,0,778,430);


// symbols:
(window.y3u17p5001g.allOkFeedback = function() {
	this.initialize();

	// Layer 1
	this.instance = new window.y3u17p5001g.allOkBg();
	this.instance.setTransform(-272.9,-29.9);

	this.addChild(this.instance);
}).prototype = p = new Container();
p.nominalBounds = new Rectangle(-272.9,-29.9,547,60);


(window.y3u17p5001g.start = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{},true);

	// Layer 1
	this.instance = new window.y3u17p5001g.btnStart();

	this.instance_1 = new window.y3u17p5001g.btnStart_over();

	this.timeline.addTween(Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).wait(1));

}).prototype = p = new MovieClip();
p.nominalBounds = new Rectangle(0,0,70,50);


(window.y3u17p5001g.progress_bar = function() {
	this.initialize();

	// Layer 1
	this.instance = new window.y3u17p5001g.progres();

	this.addChild(this.instance);
}).prototype = p = new Container();
p.nominalBounds = new Rectangle(0,0,100,6);


(window.y3u17p5001g.progres = function() {
	this.initialize(images.y3u17p5001g.progres);
}).prototype = new Bitmap();
p.nominalBounds = new Rectangle(0,0,100,6);


(window.y3u17p5001g.allOkBg = function() {
	this.initialize(images.y3u17p5001g.allOkBg);
}).prototype = new Bitmap();
p.nominalBounds = new Rectangle(0,0,547,60);


(window.y3u17p5001g.btnStart = function() {
	this.initialize(images.y3u17p5001g.btnStart);
}).prototype = new Bitmap();
p.nominalBounds = new Rectangle(0,0,70,50);


(window.y3u17p5001g.btnStart_over = function() {
	this.initialize(images.y3u17p5001g.btnStart_over);
}).prototype = new Bitmap();
p.nominalBounds = new Rectangle(0,0,70,50);


(window.y3u17p5001g.reset = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{},true);

	// Layer 1
	this.instance = new window.y3u17p5001g.reset_1();

	this.instance_1 = new window.y3u17p5001g.reset_klik();

	this.timeline.addTween(Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).wait(1));

}).prototype = p = new MovieClip();
p.nominalBounds = new Rectangle(0,0,50,50);


(window.y3u17p5001g.reset_1 = function() {
	this.initialize(images.y3u17p5001g.reset_1);
}).prototype = new Bitmap();
p.nominalBounds = new Rectangle(0,0,50,50);


(window.y3u17p5001g.reset_klik = function() {
	this.initialize(images.y3u17p5001g.reset_klik);
}).prototype = new Bitmap();
p.nominalBounds = new Rectangle(0,0,50,50);


(window.y3u17p5001g.btns = function() {
	this.initialize();

	// Layer 1
	this.b1 = new window.y3u17p5001g.btn();
	this.b1.setTransform(71.5,813.2,1,1,0,0,0,30.8,30.8);

	this.b2 = new window.y3u17p5001g.btn();
	this.b2.setTransform(71.5,739.7,1,1,0,0,0,30.8,30.8);

	this.b3 = new window.y3u17p5001g.btn();
	this.b3.setTransform(71.5,666.2,1,1,0,0,0,30.8,30.8);

	this.b4 = new window.y3u17p5001g.btn();
	this.b4.setTransform(71.5,592.7,1,1,0,0,0,30.8,30.8);

	this.b5 = new window.y3u17p5001g.btn();
	this.b5.setTransform(71.5,519.2,1,1,0,0,0,30.8,30.5);

	this.b6 = new window.y3u17p5001g.btn();
	this.b6.setTransform(180.8,818.8,1,1,0,0,0,30.8,30.8);

	this.b7 = new window.y3u17p5001g.btn();
	this.b7.setTransform(180.8,741.8,1,1,0,0,0,30.8,30.8);

	this.b8 = new window.y3u17p5001g.btn();
	this.b8.setTransform(180.8,664.8,1,1,0,0,0,30.8,30.8);

	this.b9 = new window.y3u17p5001g.btn();
	this.b9.setTransform(180.8,587.8,1,1,0,0,0,30.8,30.8);

	this.b10 = new window.y3u17p5001g.btn();
	this.b10.setTransform(180.8,510.8,1,1,0,0,0,30.8,30.8);

	this.addChild(this.b10,this.b9,this.b8,this.b7,this.b6,this.b5,this.b4,this.b3,this.b2,this.b1);
}).prototype = p = new Container();
p.nominalBounds = new Rectangle(4.7,446,181.4,376);


(window.y3u17p5001g.btn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{},true);

	// przycisk
	this.instance = new window.y3u17p5001g.btn0();
	this.instance.setTransform(-35.9,-33.9);

	this.instance_1 = new window.y3u17p5001g.btn1();
	this.instance_1.setTransform(-35.9,-33.9);

	this.instance_2 = new window.y3u17p5001g.btn2();
	this.instance_2.setTransform(-35.9,-33.9);

	this.timeline.addTween(Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_2}]},1).wait(1));

}).prototype = p = new MovieClip();
p.nominalBounds = new Rectangle(-35.9,-33.9,72,68);


(window.y3u17p5001g.btn0 = function() {
	this.initialize(images.y3u17p5001g.btn0);
}).prototype = new Bitmap();
p.nominalBounds = new Rectangle(0,0,72,68);


(window.y3u17p5001g.btn1 = function() {
	this.initialize(images.y3u17p5001g.btn1);
}).prototype = new Bitmap();
p.nominalBounds = new Rectangle(0,0,72,68);


(window.y3u17p5001g.btn2 = function() {
	this.initialize(images.y3u17p5001g.btn2);
}).prototype = new Bitmap();
p.nominalBounds = new Rectangle(0,0,72,68);


(window.y3u17p5001g.bg = function() {
	this.initialize(images.y3u17p5001g.bg);
}).prototype = new Bitmap();
p.nominalBounds = new Rectangle(0,0,720,430);