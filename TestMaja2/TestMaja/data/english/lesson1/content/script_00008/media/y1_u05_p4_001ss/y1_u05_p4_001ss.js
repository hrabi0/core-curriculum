if (!window.y1_u05_p4_001ss) { window.y1_u05_p4_001ss = {}; }

var p; // shortcut to reference prototypes

// stage content:
(window.y1_u05_p4_001ss.y1_u05_p4_001ss = function() {
	this.initialize();

	// Layer 1
	this.btnStop = new window.y1_u05_p4_001ss.stop();
	this.btnStop.setTransform(265,362);

	this.btnPlay = new window.y1_u05_p4_001ss.play_pause();
	this.btnPlay.setTransform(190,362);

	this.addChild(this.btnPlay,this.btnStop);
}).prototype = p = new Container();
p.nominalBounds = new Rectangle(190,362,143.3,68.1);


// symbols:
(window.y1_u05_p4_001ss.dot_button = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{},true);

	// Layer 6
	this.instance = new window.y1_u05_p4_001ss.dot_button_iner("synched",0,false);
	this.instance.setTransform(16.1,0,1,1,0,0,0,16.1,0);

	this.timeline.addTween(Tween.get({}).to({state:[{t:this.instance}]}).wait(3));

	// Layer 5
	this.shape = new Shape();
	this.shape.graphics.f("rgba(31,33,37,0)").p("ADwjvInfAAIAAHfIHfAAIAAnf").f();
	this.shape.setTransform(24,24);

	this.timeline.addTween(Tween.get({}).to({state:[{t:this.shape}]}).wait(3));

}).prototype = p = new MovieClip();
p.nominalBounds = new Rectangle(0,0,48,48);


(window.y1_u05_p4_001ss.stop = function() {
	this.initialize();

	// Layer 4
	this.shape = new Shape();
	this.shape.graphics.f("#4a4a4a").p("ABuhuIjcAAIAADcIDcAAIAAjc").f();
	this.shape.setTransform(33.1,32.6);

	// Layer 5
	this.shape_1 = new Shape();
	this.shape_1.graphics.f("rgba(0,0,0,0.153)").p("ABuhtIjcAAIAADcIDcAAIAAjc").f();
	this.shape_1.setTransform(35.5,35.3);

	// Layer 6
	this.bg0 = new window.y1_u05_p4_001ss.btn_bg();
	this.bg0.setTransform(33.3,33.1);

	this.addChild(this.bg0,this.shape_1,this.shape);
}).prototype = p = new Container();
p.nominalBounds = new Rectangle(0.3,0.1,68,68);


(window.y1_u05_p4_001ss.btn_bg = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{},true);

	// Layer 2
	this.shape = new Shape();
	this.shape.graphics.f("rgba(0,0,0,0.2)").p("ADRkMIAaADQAPAHAIANQAAABAAAAQACADABADQABADACAAQACABACgBQACAAABgCQAAgCAAgCQgCgFgCgEQgBAAAAAAQgKgQgSgIQgBAAAAgBIgbgDQgCAAgCABQgCACAAACQAAACABABQABACADAA").p("AAhkOQABgBAAgDQAAgCgBgBQgCgBgCgBIg7AAQgCABgCABQgBABAAACQAAADABABQACABACAAIA7AAQACAAACgB").p("ABZkNIA8AAQACAAACgBQABgBAAgDQAAgCgBgBQgCgBgCgBIg8AAQgCABgBABQgCABAAACQAAADACABQABABACAA").p("AEOixIAAA4QAAACACABQABACACAAQACAAACgCQABgBAAgCIAAg4QAAgCAAgCQAAgCgBgBQgCgBgCgBQgCABgBABQgCABAAACQAAACAAAC").p("AEWhBQgCgBgCAAQgCAAgBABQgCACAAACIAAA8QAAABACACQABABACAAQACAAACgBQABgCAAgBIAAg8QAAgCgBgC").p("AEMA6IAAA8QAAACABACQABABADAAQACAAABgBQABgCABgCIAAg8QAAgCgBgBQgCgCgCAAQgCAAgBACQgCABAAAC").p("AELCvQgBABAAADQgGAwgIAKQgBABAAACQAAACABACQABABADAAQACAAABgBQALgKAGg2QAAgDgBgBQgCgCgCAAQgCAAgCAB").p("ACPEWQABABACAAIACAAQAkABAYgGQABgBABgBQABgCAAgCQgBgCgBgBQgCgBgCAAQgWAGgjgBIgCAAQgCAAgBACQgCABAAACQAAACACAC").p("ABbERQAAgCgBgBQgCgCgCAAIg8AAQgCAAgBACQgCABAAACQAAACACACQABABACAAIA8AAQACAAACgBQABgCAAgC").p("AiWkNIA8AAQACAAABgBQACgBAAgDQAAgCgCgBQgBgBgCgBIg8AAQgCABgCABQgBABAAACQAAADABABQACABACAA").p("AkJj4QAAACAAACQABACABABQACABACgBQACAAABgCQANgTAigFQADAAABgCQABgCAAgCQAAgCgCgBQgCgBgCAAQgpAGgOAX").p("AkWi5QgBAVABAYIAAAPQAAADABABQACABACAAQACAAABgBQACgBAAgDIAAgPQgBgYABgVQABgBgCgCQgBgBgDgBQgBAAgCABQgBACgBAC").p("AkOhEQgBgBgCgBQgCABgCABQgBABAAACIAAA8QAAADABABQACABACAAQACAAABgBQACgBAAgDIAAg8QAAgCgCgB").p("AkNAzQgBgBgDAAQgCAAgBABQgBABgBADIAAA8QABACABABQABABACABQADgBABgBQABgBAAgCIAAg8QAAgDgBgB").p("AkLCvQAAgDgBgBQgBgBgDAAQgCAAgBABQgBABgBADIAAAJQADAgAKASQABACACAAQACABACgBQACgBAAgCQABgCgCgCQgJgRgCgcIAAgJ").p("AjWEFQgBABgBACQAAACAAACQABACACAAQAeAKAhgCQACgBABgBQACgCgBgCQAAgCgCgBQgBgCgCABQgfACgcgJQgBgBgDAB").p("AghEMIg8AAQgCAAgCABQgBABAAADQAAACABABQACABACABIA8AAQACgBABgBQACgBAAgCQAAgDgCgBQgBgBgCAA").f();

	this.timeline.addTween(Tween.get({}).to({state:[{t:this.shape}]}).wait(4));

	// Layer 1
	this.shape_1 = new Shape();
	this.shape_1.graphics.f("#ffcc00").p("ADekrImnAAQhNABgLAqQgMApACA+IACFiQADAnAOATQAEAHAGAEQAvAhA6gFIFHACQAzACAbgOQAbgNAMgMQAMgMAHhLIADlxQgEg9gKgQQgKgQgTgJIgkgE").f();

	this.shape_2 = new Shape();
	this.shape_2.graphics.f("#5fc2ed").p("AECknIgkgEImnAAQhNABgLAqQgMApACA+IACFiQADAnAOATQAEAHAGAEQAvAhA6gFIFHACQAzACAbgOQAbgNAMgMQAMgMAHhLIADlxQgEg9gKgQQgKgQgTgJ").f();

	this.shape_3 = new Shape();
	this.shape_3.graphics.f("#ebebeb").p("AEfkOQgKgQgTgJIgkgEImnAAQhNABgLAqQgMApACA+IACFiQADAnAOATQAEAHAGAEQAvAhA6gFIFHACQAzACAbgOQAbgNAMgMQAMgMAHhLIADlxQgEg9gKgQ").f();

	this.timeline.addTween(Tween.get({}).to({state:[{t:this.shape_1}]}).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},2).wait(1));

	// Layer 6
	this.shape_4 = new Shape();
	this.shape_4.graphics.f("rgba(0,0,0,0.047)").p("AD7lOIgigFQgEAAgDAAImQAAQh+ACgMBRIAAgBQgNAtACBFQAAAAAAABIABFNQAAACABACQAEA4AWAbQAJAOANAJQABABABABQA7AoBJgEIE0ABQBBACAjgSIAAAAQAigSAQgQQABAAAAgBQAZgPAJhmQAAgCAAgCIACleQAAgCAAgBQgEhPgQgUIAAAAQgSgegjgQQgHgDgJgB").p("ADXlEIAiAEQAGAAAHADQAeAOAQAaQAPATAEBKQAAABAAABIgDFeQAAABAAACQgJBegVAOQAAAAAAABQgPAPghAQIAAAAQggASg9gDIk1gBQhEAEg3gmQgBgBAAAAQgLgJgJgLIABABQgUgZgEg0QAAgBAAgCIgClNQAAgBAAAAQgChDANgrQALhGBxgCIGQAAQACAAADAB").f("rgba(0,0,0,0.098)").p("ADSlFImQAAQhxACgLBGQgNArACBDQAAAAAAABIACFNQAAACAAABQAEA0AUAZIgBgBQAJALALAJQAAAAABABQA3AmBEgEIE1ABQA9ADAggSIAAAAQAhgQAPgPQAAgBAAAAQAVgOAJheQAAgCAAgBIADleQAAgBAAgBQgEhKgPgTQgQgagegOQgHgDgGAAIgigEQgDgBgCAA").p("ADWk2IAiAEQAEABAEABQAaAMAOAWQANASAEBEQAAABAAABIgDFeQAAABAAABQgIBWgSANQAAAAAAABQgOANgeAPQgeAQg5gCIkygBQhCAFg0gkQAAgBgBAAQgJgHgHgJQgRgXgEgvQAAgBAAgBIgClOQgChAAMgqQAMg8BjgCIGQAAQACAAACAB").f("rgba(0,0,0,0.149)").p("ADSk3ImQAAQhjACgMA8QgMAqACBAIACFOQAAABAAABQAEAvARAXQAHAJAJAHQABAAAAABQA0AkBCgFIEyABQA5ACAegQQAegPAOgNQAAgBAAAAQASgNAIhWQAAgBAAgBIADleQAAgBAAgBQgEhEgNgSQgOgWgagMQgEgBgEgBIgigEQgCgBgCAA").p("ADUkoIAiAEQACAAACABQAWAKAMATQALARADA+IgCFeQAAABAAABQgHBOgPAMIAAAAQgNANgbAOQgcAOg1gCIkzgBQg8AEgxghIAAgBQgIgFgFgIIAAAAQgQgUgDgqQAAgBAAAAIgClPQgBg9ALgoQALgzBWAAIGQAAQABAAABAA").f("rgba(0,0,0,0.2)").p("ADSkoImQAAQhWAAgLAzQgLAoABA9IACFPQAAAAAAABQADAqAQAUIAAAAQAFAIAIAFIAAABQAxAhA8gEIEzABQA1ACAcgOQAbgOANgNIAAAAQAPgMAHhOQAAgBAAgBIACleQgDg+gLgRQgMgTgWgKQgCgBgCAAIgigEQgBAAgBAA").f();
	this.shape_4.setTransform(1,1);

	this.timeline.addTween(Tween.get({}).to({state:[{t:this.shape_4}]}).to({state:[{t:this.shape_4}]},1).wait(3));

}).prototype = p = new MovieClip();
p.nominalBounds = new Rectangle(-32.9,-32.9,68,68);


(window.y1_u05_p4_001ss.play_pause = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{},true);

	// Layer 6
	this.bg0 = new window.y1_u05_p4_001ss._1_play();

	this.bg1 = new window.y1_u05_p4_001ss._2_pause();

	this.timeline.addTween(Tween.get({}).to({state:[{t:this.bg0}]}).to({state:[{t:this.bg1}]},1).wait(1));

}).prototype = p = new MovieClip();
p.nominalBounds = new Rectangle(0,0,68,68);


(window.y1_u05_p4_001ss._1_play = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{},true);

	// Layer 2
	this.shape = new Shape();
	this.shape.graphics.f("#474747").p("AhnCMIDgiYIjxh+IAREW").f();
	this.shape.setTransform(34.6,33.1);

	this.timeline.addTween(Tween.get({}).to({state:[{t:this.shape}]}).wait(4));

	// Layer 4
	this.shape_1 = new Shape();
	this.shape_1.graphics.f("rgba(0,0,0,0.149)").p("AhnCMIDgiYIjxh/IAREX").f();
	this.shape_1.setTransform(37,34.7);

	this.timeline.addTween(Tween.get({}).to({state:[{t:this.shape_1}]}).wait(4));

	// Layer 6
	this.instance = new window.y1_u05_p4_001ss.btn_bg("single",0);
	this.instance.setTransform(33,33);

	this.timeline.addTween(Tween.get(this.instance).wait(1).to({startPosition:1},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:3},0).wait(1));

}).prototype = p = new MovieClip();
p.nominalBounds = new Rectangle(0,0,68,68);


(window.y1_u05_p4_001ss._2_pause = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{},true);

	// Layer 5
	this.shape = new Shape();
	this.shape.graphics.f("#4a4a4a").p("AB2h1IhVAAIAADsIBVAAIAAjs").p("Aghh1IhVAAIAADsIBVAAIAAjs").f();
	this.shape.setTransform(33,32.6);

	this.timeline.addTween(Tween.get({}).to({state:[{t:this.shape}]}).wait(4));

	// Layer 4
	this.shape_1 = new Shape();
	this.shape_1.graphics.f("rgba(0,0,0,0.153)").p("AB3h1IhVAAIAADrIBVAAIAAjr").p("Aghh1IhVAAIAADrIBVAAIAAjr").f();
	this.shape_1.setTransform(35.5,34.6);

	this.timeline.addTween(Tween.get({}).to({state:[{t:this.shape_1}]}).wait(4));

	// Layer 6
	this.instance = new window.y1_u05_p4_001ss.btn_bg("synched",1,false);
	this.instance.setTransform(33,33);

	this.timeline.addTween(Tween.get(this.instance).wait(1).to({startPosition:0},0).wait(1).to({startPosition:1},0).wait(1).to({startPosition:3},0).wait(1));

}).prototype = p = new MovieClip();
p.nominalBounds = new Rectangle(0,0,68,68);


(window.y1_u05_p4_001ss.dot_button_iner = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{},true);

	// Layer 8
	this.shape = new Shape();
	this.shape.graphics.f("rgba(0,0,0,0.2)").p("AAihkQAWAIASARQABACADAAQACAAABgCQABgBABgCQgBgCgBgCQgTgTgZgIQgCAAgCAAQgCABAAACQgBACABACQABACACAA").p("AgPhyQgXAEgUALIAAAAQgDACgDACQgCABAAACQAAADABABQABACACAAQACABACgCQADgBACgDIAAAAQASgKAVgDQACAAACgCQABgBgBgDQAAgBgCgCQgBgBgCAA").p("ABlgzQgBAAgCABQgCABgBABQAAADAAACQAKAUAAAXQAAABAAAAQAAACABABQABACADAAQACAAABgCQABgBABgCQAAAAAAgBQgBgagJgWQgBgBgDgC").p("Ahag6QAAgCgCgBQgBgBgDAAQgBABgCACQgMAWgDAbQAAADABABQACACACAAQACAAABgCQACgBAAgBQACgaAMgUQABgCgBgC").p("AhlAiQgBgBgDAAQgBACgBABQgBACAAACQAJAXASASQACABABABQACACACAAQACAAACgCQABgBAAgCQAAgCgBgCQgCgBgCgCQgQgQgIgVQgBgBgCgB").p("ABRBRQANgNAHgPQABgCgBgCQAAgBgCgCQgCAAgCAAQgBABgBABQgIAOgLALQgFAGgGAEQgCACAAACQAAACABACQACABACAAQACABABgCQAGgFAGgF").p("AghBuQAQAEARABQAKAAALgCQACAAABgCQABgCAAgCQAAgCgCgBQgCgBgCAAQgJABgKABQgQAAgOgFQgCAAgCABQgCABgBABQAAACABACQABACACAB").f();
	this.shape.setTransform(24,24.1);

	this.timeline.addTween(Tween.get({}).to({state:[{t:this.shape}]}).wait(3));

	// Layer 2
	this.shape_1 = new Shape();
	this.shape_1.graphics.f("#ffcc00").p("AAAiVQgpAAgiAUQgQAJgOAOQgsArAAA/QAAA+AsAsQAsAsA9AAQApAAAhgTQARgKAPgPQAsgsAAg+QAAg/gsgrQgqgrhAAA").f();
	this.shape_1.setTransform(24,24);

	this.shape_2 = new Shape();
	this.shape_2.graphics.f("#b1d621").p("ACWAAQAAg/gsgrQgqgrhAAAQg9AAgsArQgsArAAA/QAAA+AsAsQAsAsA9AAQBAAAAqgsQAsgsAAg+").f();
	this.shape_2.setTransform(24,24);

	this.shape_3 = new Shape();
	this.shape_3.graphics.f("#5fc2ed").p("ACWAAQAAg/gsgrQgqgrhAAAQg9AAgsArQgsArAAA/QAAA+AsAsQAsAsA9AAQBAAAAqgsQAsgsAAg+").f();
	this.shape_3.setTransform(24,24);

	this.timeline.addTween(Tween.get({}).to({state:[{t:this.shape_1}]}).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).wait(1));

	// Layer 14
	this.shape_4 = new Shape();
	this.shape_4.graphics.f("rgba(0,0,0,0.031)").p("ACgAAQAAhDgvguQgPgPgSgLQgMgHgNgEQgagJgdAAQgrAAglAVQgRAKgPAOIAAABQgvAuAABDQAABCAvAvIAAgBQAvAwBBAAQAsAAAkgUQASgLAPgQIABgBQAkgkAIgwQACgNAAgOQAAgBAAAA").f();
	this.shape_4.setTransform(24,24);

	this.timeline.addTween(Tween.get({}).to({state:[{t:this.shape_4}]}).wait(3));

	// Layer 13
	this.shape_5 = new Shape();
	this.shape_5.graphics.f("rgba(0,0,0,0.031)").p("ACqABIAAgBQAAhGgygyQgQgQgTgLQgMgIgOgEQgcgKgfAAQguAAgnAXQgSAKgQAPIAAABQgyAyAABGQAABHAyAxIAAgBQAyAzBFAAQAvAAAmgVQATgMAQgRIAAgBQAngmAJgzQACgOAAgP").f();
	this.shape_5.setTransform(24,24);

	this.timeline.addTween(Tween.get({}).to({state:[{t:this.shape_5}]}).wait(3));

	// Layer 12
	this.shape_6 = new Shape();
	this.shape_6.graphics.f("rgba(0,0,0,0.031)").p("AC0ABQAAgBAAAAQAAhLg1g1QgRgRgUgLQgOgIgPgFQgcgKghAAQgyAAgoAYQgTALgRAQIAAAAQg1A1AABLQAABLA1A0IAAgBQA0A2BKAAQAxAAAogXQAVgMARgSIABgBQAogpAJg2QADgPAAgP").f();
	this.shape_6.setTransform(24,24);

	this.timeline.addTween(Tween.get({}).to({state:[{t:this.shape_6}]}).wait(3));

}).prototype = p = new MovieClip();
p.nominalBounds = new Rectangle(6,6,36,36);