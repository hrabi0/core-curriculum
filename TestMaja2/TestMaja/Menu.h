//
//  Menu.h
//  TestMaja
//
//  Created by gkowalewski on 13.11.2013.
//  Copyright (c) 2013 gkowalewski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Menu : UIView <UIWebViewDelegate> {
    id _delegate;
}

@property (nonatomic, assign, getter=getDelegate) id delegate;

@end
