//
//  ViewController.m
//  TestMaja
//
//  Created by gkowalewski on 21.06.2013.
//  Copyright (c) 2013 gkowalewski. All rights reserved.
//

#import "ViewController.h"
#import "Menu.h"

@interface ViewController () {
    Menu *_menu;
    int _statusBarMode;
}

@end

@implementation ViewController

- (UIStatusBarStyle)preferredStatusBarStyle
{
    if (_statusBarMode == 0) {
        return UIStatusBarStyleDefault;
    }
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad
{
    _statusBarMode = 1;
    [super viewDidLoad];

    _menu = [[Menu alloc] initWithFrame:CGRectMake(0, 0, 1024, 768)];
    _menu.delegate = self;
    [self.view addSubview:_menu];
    /*
    float wid = 1024;//fmax(self.view.frame.size.width, self.view.frame.size.height);
    float hei = 1024;//fmin(self.view.frame.size.width, self.view.frame.size.height);
    
    UIWebView *web = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, wid, hei)];
    [self.view addSubview:web];
    NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"data/english/lessons/lesson/proxy/scorm13/script_00003.emt" ofType:@"html"]];
    for (id subview in web.subviews)
        if ([[subview class] isSubclassOfClass:[UIScrollView class]])
            ((UIScrollView*)subview).scrollEnabled=NO;
    [web loadRequest:[NSURLRequest requestWithURL:url]];
     */
}

-(void)setStatusBarMode:(int)num {
    _statusBarMode = num;
    if (_statusBarMode == 0) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    } else {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    }
    
    [self setNeedsStatusBarAppearanceUpdate];
}


-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return (toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation == UIInterfaceOrientationLandscapeRight);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
