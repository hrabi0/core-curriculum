//
//  Menu.m
//  TestMaja
//
//  Created by gkowalewski on 13.11.2013.
//  Copyright (c) 2013 gkowalewski. All rights reserved.
//

#import "Menu.h"
#import "ViewController.h"

@interface Menu() {
    UIWebView *_web;;
    UIImageView *_white;
    UIButton *_homeBtn;
}

-(void) addButtons;
-(void) addWeb;
-(void) loadWeb:(int)num;

@end

@implementation Menu

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        UIImageView *back = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 1024, 768)];
        [back setImage:[UIImage imageNamed:@"data/gfx/toc_2048x1536.png"]];
        [self addSubview:back];
        
        [_delegate setStatusBarMode:1];

        _white = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 1024, 20)];
        [_white setImage:[UIImage imageNamed:@"data/gfx/white_1024x20.png"]];
        [self addSubview:_white];
        _white.hidden = YES;
        
        [self addButtons];
        [self addWeb];
    }
    return self;
}

-(void)addButtons {
    UIButton *but;
    UIImage *img1, *img2;
    
    NSArray *arrUp = [[NSArray alloc] initWithObjects:
                    @"data/gfx/btn_toc_y1_u05_up.png",
                    @"data/gfx/btn_toc_y2_u07_up.png",
                    @"data/gfx/btn_toc_y3_u17_up.png",
                    @"data/gfx/btn_toc_y4_u03_up.png",
                    @"data/gfx/btn_toc_y5_u07_up.png",
                    @"data/gfx/btn_toc_y6_u04_up.png",
                    nil];
    NSArray *arrDown = [[NSArray alloc] initWithObjects:
                      @"data/gfx/btn_toc_y1_u05_down.png",
                      @"data/gfx/btn_toc_y2_u07_down.png",
                      @"data/gfx/btn_toc_y3_u17_down.png",
                      @"data/gfx/btn_toc_y4_u03_down.png",
                      @"data/gfx/btn_toc_y5_u07_down.png",
                      @"data/gfx/btn_toc_y6_u04_down.png",
                      nil];
    float pos[6][2] = {{201,222},{201,272},{201,322},{541,322},{541,372},{541,422}};
    
    for (int i = 0; i<arrUp.count; i++) {
        img1 = [UIImage imageNamed:[NSString stringWithFormat:@"%@",arrUp[i]]];
        img2 = [UIImage imageNamed:[NSString stringWithFormat:@"%@",arrDown[i]]];
        
        but = [UIButton buttonWithType:UIButtonTypeCustom];
        but.frame = CGRectMake(pos[i][0], pos[i][1], img1.size.width/2, img1.size.height/2);
        but.tag = i;
        [but setImage:img1 forState:UIControlStateNormal];
        [but setImage:img2 forState:UIControlStateHighlighted];
        [but addTarget:self action:@selector(butPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:but];
    }
    
    img1 = [UIImage imageNamed:@"data/gfx/btn_home_app_up.png"];
    img2 = [UIImage imageNamed:@"data/gfx/btn_home_app_dow.png"];
    
    _homeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _homeBtn.frame = CGRectMake(0,6+20,img1.size.width/2, img1.size.height/2);
    [_homeBtn setImage:img1 forState:UIControlStateNormal];
    [_homeBtn setImage:img2 forState:UIControlStateHighlighted];
    [_homeBtn addTarget:self action:@selector(homePressed:) forControlEvents:UIControlEventTouchUpInside];
    _homeBtn.hidden = YES;
    [self addSubview:_homeBtn];
    
    [arrDown release];
    [arrUp release];
    
}

-(void)addWeb {
    float wid = 1024;//fmax(self.view.frame.size.width, self.view.frame.size.height);
    float hei = 768;//fmin(self.view.frame.size.width, self.view.frame.size.height);
    
    _web = [[UIWebView alloc] initWithFrame:CGRectMake(0, 20, wid, hei)];
    _web.hidden = YES;
    _web.delegate = self;
    [self addSubview:_web];
    
    [self bringSubviewToFront:_homeBtn];
}

-(void)loadWeb:(int)num {
    [_delegate setStatusBarMode:0];
    
    NSArray *arrUrl = [[NSArray alloc] initWithObjects:
                       @"lesson1/proxy/scorm13/script_00008.emt",
                       @"lesson2/proxy/scorm13/script_00007.emt",
                       @"lesson3/proxy/scorm13/script_00017.emt",
                       @"lesson4/proxy/scorm13/script_00003.emt",
                       @"lesson5/proxy/scorm13/script_00007.emt",
                       @"lesson6/proxy/scorm13/script_00004.emt",
                       nil];
    
    NSString *str = [NSString stringWithFormat:@"data/english/%@",arrUrl[num]];
    
    NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:str ofType:@"html"]];
    
    for (id subview in _web.subviews)
        if ([[subview class] isSubclassOfClass:[UIScrollView class]])
            ((UIScrollView*)subview).scrollEnabled=NO;
    
    [_web loadRequest:[NSURLRequest requestWithURL:url]];
   
    
    [arrUrl release];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView {
    _homeBtn.hidden = NO;
    _white.hidden = NO;
    _web.hidden = NO;
    
}

-(void)homePressed:(UIButton*)sender {
    _homeBtn.hidden = YES;
     _white.hidden = YES;
    _web.hidden = YES;
    [_delegate setStatusBarMode:1];
    
}

-(void)butPressed:(UIButton*)sender {
    [self loadWeb:sender.tag];
}

-(id) getDelegate {
	return _delegate;
}

-(void) setDelegate:(id)delegate {
	_delegate = delegate;
}

@end
