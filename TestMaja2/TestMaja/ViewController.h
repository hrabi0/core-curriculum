//
//  ViewController.h
//  TestMaja
//
//  Created by gkowalewski on 21.06.2013.
//  Copyright (c) 2013 gkowalewski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController {
    
}

-(void) setStatusBarMode:(int)num;

@end
