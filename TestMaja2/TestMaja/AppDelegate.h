//
//  AppDelegate.h
//  TestMaja
//
//  Created by gkowalewski on 21.06.2013.
//  Copyright (c) 2013 gkowalewski. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
